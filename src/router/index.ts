import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/Home.vue'
import Play from '../components/Play.vue'
import Settings from '../components/Settings.vue'
import Test from '../components/Test.vue'

const routes = [
    { path: '/edh-tracker', name: 'Home', component: Home },
    { path: '/edh-tracker/play', name: 'Play', component: Play },
    { path: '/edh-tracker/settings', name: 'Settings', component: Settings },
    { path: '/edh-tracker/test', name: 'Test', component: Test }
    
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

export default router